package pl.sdacademy.bankomat;

import java.util.Arrays;

public class ATM {
    private int[] notes;

    public ATM(int... notes) {
        Arrays.sort(notes);
        reverseSort(notes);
        this.notes = notes;
    }

    public int[] getNotes() {
        return notes;
    }

    public int[] withdraw(int money) {
        int[] givenNotes = {};
        int sumOfGivenNotes = 0; // I could have counted that each time, but this seems like a better choice
        int i = -1;
        while (sumOfGivenNotes < money && i < notes.length-1) {
            i++;
            if (money - sumOfGivenNotes < notes[i]) {
                continue;
            }
            sumOfGivenNotes += notes[i];
            givenNotes = add(givenNotes, notes[i]);
        }
        if (sumOfGivenNotes != money) {
            givenNotes = new int[]{};
        } else {
            for (int givenNote : givenNotes) {
                for (int j = 0; j < notes.length; j++) {
                    if (notes[j] == givenNote) {
                        notes = remove(notes, j);
                        break;
                    }
                }
            }
        }
        return givenNotes;
    }

    private void reverseSort(int[] array) {
        if (array == null) {
            return;
        }
        int i = 0;
        int j = array.length - 1;
        int tmp;
        while (j > i) {
            tmp = array[j];
            array[j] = array[i];
            array[i] = tmp;
            j--;
            i++;
        }
    }

    private int[] add(int[] array, int newValue) {
        array = Arrays.copyOf(array, array.length + 1);
        array[array.length - 1] = newValue;
        return array;
    }

    private int[] remove(int[] array, int index) {
        int[] newArray = new int[array.length - 1];
        if (index >= 0) System.arraycopy(array, 0, newArray, 0, index);
        if (newArray.length - index >= 0) System.arraycopy(array, index + 1, newArray, index, newArray.length - index);
        return newArray;
    }

}
