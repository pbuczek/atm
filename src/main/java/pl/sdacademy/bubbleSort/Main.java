package pl.sdacademy.bubbleSort;

public class Main {
    public static void bubbleSort(int[] array) {
        int holdValue;
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - (1 + i); j++) {
                if (array[j] > array[j + 1]) {
                    holdValue = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = holdValue;
                }
            }
        }
    }
}
