package pl.sdacademy.bankomat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ATMTest {
    ATM atm;

    @BeforeEach
    public void setUp() {
        atm = new ATM(100, 20, 20, 10, 200, 50, 50, 50);
    }

    @Test
    public void should_Be_Reversed_Sorted_After_Construction() {
        assertThat(atm.getNotes()).containsExactly(200, 100, 50, 50, 50, 20, 20, 10);
    }

    @Test
    public void should_Return_Empty_Array_When_Negative() {
        assertThat(atm.withdraw(-20)).isEmpty();
    }

    @Test
    public void should_Not_Change_Notes_When_Negative() {
        int[] notesBefore = atm.getNotes();
        atm.withdraw(-20);
        assertThat(atm.getNotes()).containsExactly(notesBefore);
    }

    @Test
    public void should_Return_Empty_Array_When_Zero() {
        assertThat(atm.withdraw(0)).isEmpty();
    }

    @Test
    public void should_Not_Change_Notes_When_Zero() {
        int[] notesBefore = atm.getNotes();
        atm.withdraw(0);
        assertThat(atm.getNotes()).containsExactly(notesBefore);
    }

    @Test
    public void should_Return_Correct_Array_When_Possible_One_Note() {
        assertThat(atm.withdraw(20)).containsExactly(20);
    }

    @Test
    public void should_Change_Notes_One_Note() {
        atm.withdraw(20);
        assertThat(atm.getNotes()).containsExactly(200, 100, 50, 50, 50, 20, 10);
    }

    @Test
    public void should_Return_Lowest_Number_Of_Notes_Possible() {
        assertThat(atm.withdraw(200)).containsExactly(200);
    }

    @Test
    public void should_Change_Notes_By_Giving_Lowest_Number_Of_Notes_Possible() {
        atm.withdraw(200);
        assertThat(atm.getNotes()).containsExactly(100, 50, 50, 50, 20, 20, 10);
    }

    @Test
    public void should_Return_Correct_Array_When_Possible_Many_Different_Notes() {
        assertThat(atm.withdraw(330)).containsExactly(200, 100, 20, 10);
    }

    @Test
    public void should_Change_Notes_When_Possible_Many_Different_Notes() {
        atm.withdraw(330);
        assertThat(atm.getNotes()).containsExactly(50, 50, 50,20);
    }

    @Test
    public void should_Return_Correct_Array_When_Possible_Multiple_Same_Notes() {
        assertThat(atm.withdraw(90)).containsExactly(50, 20, 20);
    }

    @Test
    public void should_Change_Notes_When_Possible_Multiple_Same_Notes() {
        atm.withdraw(90);
        assertThat(atm.getNotes()).containsExactly(200, 100, 50, 50, 10);
    }

    @Test
    public void should_Return_Correct_Array_When_Possible_AlmostAllNotes() {
        assertThat(atm.withdraw(490)).containsExactly(200, 100, 50, 50, 50, 20, 20);
    }

    @Test
    public void should_Change_Notes_When_Possible_AlmostAllNotes() {
        atm.withdraw(490);
        assertThat(atm.getNotes()).containsExactly(10);
    }

    @Test
    public void should_Return_Correct_Array_When_Possible_AllNotes() {
        assertThat(atm.withdraw(500)).containsExactly(200, 100, 50, 50, 50, 20, 20, 10);
    }

    @Test
    public void should_Change_Notes_When_Possible_AllNotes() {
        atm.withdraw(500);
        assertThat(atm.getNotes()).isEmpty();
    }

    @Test
    public void should_Return_Empty_Array_When_Money_Not_Round() {
        assertThat(atm.withdraw(35)).isEmpty();
    }

    @Test
    public void should_Not_Change_Notes_When_Money_Not_Round() {
        int[] notesBefore = atm.getNotes();
        atm.withdraw(35);
        assertThat(atm.getNotes()).containsExactly(notesBefore);
    }

    @Test
    public void should_Return_Empty_Array_When_Money_Cannot_Be_Given_Too_Much() {
        assertThat(atm.withdraw(510)).isEmpty();
    }

    @Test
    public void should_Not_Change_Notes_When_Money_Cannot_Be_Given_Too_Much() {
        int[] notesBefore = atm.getNotes();
        atm.withdraw(510);
        assertThat(atm.getNotes()).containsExactly(notesBefore);
    }

    @Test
    public void should_Return_Empty_Array_When_Money_Cannot_Be_Given_Lacking_Specific_Notes() {
        atm = new ATM(100,500, 10, 10, 50);
        assertThat(atm.withdraw(80)).isEmpty();
    }

    @Test
    public void should_Not_Change_Notes_When_Money_Cannot_Be_Given_Lacking_Specific_Notes() {
        atm = new ATM(100,500, 10, 10, 50);
        int[] notesBefore = atm.getNotes();
        assertThat(atm.withdraw(80)).isEmpty();
        assertThat(atm.getNotes()).containsExactly(notesBefore);
    }

}
