package pl.sdacademy.bubbleSort;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.sdacademy.bubbleSort.Main.bubbleSort;

public class MainTest {
    @Test
    public void should_Sort_Normal() {
        int[] array = {-2_147_483_648, 2_147_483_647, 3, 0, 7, 11, 1, -2, 3};
        bubbleSort(array);
        assertThat(array).containsExactly(-2_147_483_648, -2, 0, 1, 3, 3, 7, 11, 2_147_483_647);
    }

    @Test
    public void should_Leave_The_Same_When_Sorted_Already() {
        int[] array = {-212, -3, 0, 1, 19, 20199};
        bubbleSort(array);
        assertThat(array).containsExactly(-212, -3, 0, 1, 19, 20199);
    }

    @Test
    public void should_Reverse_When_Sorted_From_Highest_To_Lowest() {
        int[] array = {12321, 212, 1, 0, -1, -123, -20000};
        bubbleSort(array);
        assertThat(array).containsExactly(-20000, -123, -1, 0, 1, 212, 12321);
    }

    @Test
    public void should_Leave_The_Same_When_One_Element() {
        int[] array = {-15};
        bubbleSort(array);
        assertThat(array).containsExactly(-15);
    }

    @Test
    public void should_Leave_The_Same_When_Zero_Elements() {
        int[] array = {};
        bubbleSort(array);
        assertThat(array).isEmpty();
    }
}
